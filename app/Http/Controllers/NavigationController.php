<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NavigationController extends Controller{

    public function home(){
        return view('pages.home');
    }
    public function services(){
        return view('pages.services');
    }
    public function about(){
        return view('pages.about');
    }
    public function contact(){
        return view('pages.contact');
    }
    public function faq(){
        return view('pages.faq');
    }

    public function sign_up(){
        return view('pages.sign_up');
    }
    public function submited(){
        return view('pages.submited');
    }

}