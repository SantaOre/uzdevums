<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Uzdevums;

class SignupController extends Controller
{
    
    function addData(Request $req){
        $req->validate([
            'name'=>'required',
            'email'=>'required | email'
        ]);

        $uzdevums = new Uzdevums;
        $uzdevums->name=$req->name;
        $uzdevums->email=$req->email;
        $uzdevums->save();
        return redirect('/submited');
    }
}
