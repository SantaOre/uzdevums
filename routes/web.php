<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\NavigationController;
use App\Http\Controllers\SignupController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 
    [NavigationController::class, 'home'])->name('home');

Route::get('/services', 
    [NavigationController::class, 'services'])->name('services');

Route::get('/about', 
    [NavigationController::class, 'about'])->name('about');
    
Route::get('/contact', 
    [NavigationController::class, 'contact'])->name('contact');

Route::get('/faq', 
    [NavigationController::class, 'faq'])->name('faq');


    
Route::get('/sign_up', 
    [NavigationController::class, 'sign_up'])->name('sign_up');

Route::post('/sign_up', 
    [SignupController::class, 'addData'])->name('sign_up');

Route::get('/submited', 
    [NavigationController::class, 'submited'])->name('submited');



