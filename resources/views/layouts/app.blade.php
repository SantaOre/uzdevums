<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Magecode</title>
    
</head>
<body>
    <header>
        <nav>
            <h1>LOGO</h1>
            <ul>
                <li> 
                    <a href="/" class="{{ request()->is('/') ? 'active' : '' }}">Home</a> 
                </li>
                <li>
                    <a href="services" class="{{ request()->is('services') ? 'active' : '' }}">Services</a>
                </li>
                <li>
                    <a href="about" class="{{ request()->is('about') ? 'active' : '' }}">About</a>
                </li>
                <li>
                    <a href="contact" class="{{ request()->is('contact') ? 'active' : '' }}">Contact</a>
                </li>
                <li>
                    <a href="faq" class="{{ request()->is('faq') ? 'active' : '' }}">FAQ</a>
                </li>

                <li>
                    <a href="sign_up" class="sign {{ request()->is('sign_up') ? 'active' : '' }}">SIGN UP</a>
                </li>
            </ul>

            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>

        </nav>
    <header>

    @yield('content')

<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>