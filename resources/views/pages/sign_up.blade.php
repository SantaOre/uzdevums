@extends('...layouts.app')

@section('content')
<h1 class="title">Sign Up</h1><br><br>
<div>
    <form action="/sign_up" method="POST">
        @csrf
        <input type="text" name="name" value='{{old("name")}}' placeholder="Enter Name"> <br> <br>
            <span style="color: red"> @error('name'){{$message}}@enderror </span><br>

        <input type="text" name="email" value='{{old("email")}}' placeholder="Enter Email"> <br> <br>
            <span style="color: red"> @error('email'){{$message}}@enderror </span><br>

        <br><button type="submit" class="submit" >Submit</button>
    </form>
</div>
@endsection
